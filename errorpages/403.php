<?php
    require $_SERVER['DOCUMENT_ROOT'] . '/vendor/parsedown/parsedown.php';
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width initial-scale=1">
<title>Forbidden</title>
</head>
<body>
    <?php
        $Parsedown = new Parsedown();
        echo $Parsedown->text(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/errorpages/error-403.md'));

    ?>
</body>
</html>