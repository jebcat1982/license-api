<?php
    require $_SERVER['DOCUMENT_ROOT'] . '/vendor/parsedown/parsedown.php';
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width initial-scale=1">
<title>Open Source License API Documentation</title>
<style>
<?php echo(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/docs/stylesheet.css')); ?>
<?php /* Uncomment for dark theme echo(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/docs/stylesheet-dark.css')); */ ?>

</style>
</head>
<body>
    <?php
        $Parsedown = new Parsedown();
        echo $Parsedown->text(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/DOCUMENTATION.md'));

    ?>
</body>
</html>