<?php

    /**
     * Returns a list of available endpoints
     */
    function get_endpoints()
    {
        $endpoints = './licenses/endpoints.txt';
        if (file_exists($endpoints))
        {
            // Send response code 200 OK and the content type
            http_response_code(200);
            header("Content-type: text/plain");

            //Print the contents of the endpoints list
            readfile($endpoints);
        }
        else
        {
            // Send response code 503 service unavailable and the content type
            http_response_code(503);
            header("Content-type: text/plain");

            // Let the user know to contact admins since there is an issue
            echo("ERROR - Endpoint list not found! Contact the site administrator if the problem persists.");
        }
    }

    /**
     * Returns a list of available license IDs. Limited to 5 licenses per row for readability.
     */
    function get_list()
    {
        $database = './licenses/licenses.txt';
        if (file_exists($database))
        {
            // Send response code 200 OK and the content type
            http_response_code(200);
            header("Content-type: text/plain");

            // Get the file contents
            $contents = file($database);
            $displayed_text = "";

            $max_per_line = 5; // Limit to 5 licenses per line to improve readability in terminals
            $current_item = 0; // Index of license on current line
            foreach ($contents as $line_num => $line) 
            {
                //Only append text before the first space
                $id = strtok($line, ' ');
                $displayed_text .= $id . ', ';
                
                //Move to the next index
                ++$current_item;
                if ($current_item == $max_per_line)
                {
                    // We hit the maximum number of items on this row, so move to the next line
                    $displayed_text .= "\n";
                    $current_item = 0;
                }
            }

            // Remove the last comma and add a newline
            $displayed_text = rtrim($displayed_text, ", ");
            $displayed_text .= "\n";

            echo $displayed_text;
        }
        else
        {
            // Send response code 503 service unavailable and the content type
            http_response_code(503);
            header("Content-type: text/plain");

            // Let the user know to contact admins since there is an issue
            echo("ERROR - License database not found! Contact the site administrator if the problem persists.");
        }
    }

    /**
     * Returns a detailed list of available licenses
     */
    function get_list_full()
    {
        $database = './licenses/licenses.txt';
        if (file_exists($database))
        {
            // Send response code 200 OK and the content type
            http_response_code(200);
            header('Content-Type: text/plain');

            // Print the contents of the pre-generated license list
            readfile($database);
        }
        else
        {
            // Send response code 503 service unavailable and the content type
            http_response_code(503);
            header("Content-type: text/plain");

            // Let the user know to contact admins since there is an issue
            echo("ERROR - License database not found! Contact the site administrator if the problem persists.");
        }
    }

    /**
     * Returns a single license using its ID
     */
    function get_license($license)
    {
        if (file_exists($license))
        {
            // Send response code 200 OK and the content type
            http_response_code(200);
            header("Content-type: text/plain");

            //Print the contents of the license file
            readfile($license);
            //Add a new line to the end of the output
            echo ("\n");
        }
        else
        {
            // Send response code 404 not found and the content type
            http_response_code(404);
            header("Content-type: text/plain");

            // Let the user know that the requested license is not found
            echo("ERROR - " . strtolower($license) . " is undefined. Use list command to see defined license types.");
        }
    }

?>