<?php
    //Include our API functions
    include('./functions.php');

    // Kickstart the framework
    $f3=require('./vendor/fatfree-3.6/lib/base.php');

    $f3->route('GET /',
        function() 
        {
            $page = $_SERVER['DOCUMENT_ROOT'] . '/docs/index.php';
            if (file_exists($page))
            {
                include($_SERVER['DOCUMENT_ROOT'] . '/docs/index.php');
            }
            else
            {
                echo('No default page defined.');
            }
        }
    );

    $f3->route('GET /api',
        function() 
        {
            get_endpoints();
        }
    );

    $f3->route('GET /api/list',
        function() 
        {
            get_list();
        }
    );

    $f3->route('GET /api/list/full',
        function() 
        {
            get_list_full();
        }
    );

    $f3->route('GET /api/@license',
        function($f3) 
        {
            $license = "./licenses/raw/" . strtolower($f3->get('PARAMS.license')) . '.txt';
            get_license($license);
        }
    );

    $f3->run();
?>